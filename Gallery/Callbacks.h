#pragma once
#include "Album.h"
#include <vector>

using std::list;
using std::string;
using std::vector;

class Callbacks
{
public:
	static int count(void* data, int argc, char** argv, char** azColName);
	static int listAlbums(void* data, int argc, char** argv, char** azColName);
	static int linkPicturesToAlbum(void* data, int argc, char** argv, char** azColName);
	static int linkTagsToPicture(void* data, int argc, char** argv, char** azColName);
	static int vecUsers(void* data, int argc, char** argv, char** azColName);
};