#include "DatabaseAccess.h"

DatabaseAccess::DatabaseAccess()
{
	dbFileName = "galleryDB.sqlite";
}

DatabaseAccess::DatabaseAccess(string dbFileName)
{
	this->dbFileName = dbFileName;
}

DatabaseAccess::~DatabaseAccess()
{

}

bool DatabaseAccess::open()
{
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);

	try
	{
		if (res != SQLITE_OK)
		{
			db = nullptr;
			throw "Failed to open DB";
		}
		executeSQL("PRAGMA foreign_keys = ON;", db); //enabling foreign keys
		if (doesFileExist == -1)
		{
			//creating tables
			executeSQL("CREATE TABLE Users (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT NOT NULL);", db);
			executeSQL("CREATE TABLE Albums (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT NOT NULL, Creation_date DATE NOT NULL, User_id INTEGER NOT NULL, FOREIGN KEY(User_id) REFERENCES Users(Id));", db);
			executeSQL("CREATE TABLE Pictures (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT NOT NULL, Location TEXT NOT NULL, Creation_date DATE NOT NULL, Album_id INTEGER NOT NULL, FOREIGN KEY(Album_id) REFERENCES Albums(Id));", db);
			executeSQL("CREATE TABLE Tags (Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Picture_id INTEGER NOT NULL, User_id INTEGER NOT NULL, FOREIGN KEY(Picture_id) REFERENCES Pictures(Id), FOREIGN KEY(User_id) REFERENCES Users(Id));", db);
		}
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(db);
	db = nullptr;
}

void DatabaseAccess::clear()
{
	albumsLists.clear();
	users.clear();
	picturesLists.clear();
}

// album related
const std::list<Album>& DatabaseAccess::getAlbums()
{
	list<Album> albumsList;
	executeSQL("SELECT * FROM Albums;", Callbacks::listAlbums, (void*)&albumsList, db);
	linkAlbumsToPicturesToTags(albumsList);
	albumsLists.push_back(albumsList);
	return albumsLists.back();
}

const std::list<Album>& DatabaseAccess::getAlbumsOfUser(const User& user)
{
	list<Album> albumsList;
	executeSQL("SELECT * FROM Albums WHERE User_id = " + to_string(user.getId()) + ";", Callbacks::listAlbums, (void*)&albumsList, db);
	linkAlbumsToPicturesToTags(albumsList);
	albumsLists.push_back(albumsList);
	return albumsLists.back();
}

void DatabaseAccess::createAlbum(const Album& album)
{
	snprintf(buffer, 300, "INSERT INTO Albums (Name, Creation_date, User_id) VALUES ('%s', '%s', %d);", album.getName().c_str(), album.getCreationDate().c_str(), album.getOwnerId());
	executeSQL(buffer, db);
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	executeSQL("DELETE FROM Albums WHERE Name = \"" + albumName + "\" AND User_id = " + to_string(userId) + ";", db);
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	list<Album> albumsList;
	snprintf(buffer, 300, "SELECT * FROM Albums WHERE Name = '%s' AND User_id = %d;", albumName.c_str(), userId);
	executeSQL(buffer, Callbacks::listAlbums, (void*)&albumsList, db);
	return !albumsList.empty();
}

Album& DatabaseAccess::openAlbum(const std::string& albumName)
{
	list<Album> albumsList;
	snprintf(buffer, 300, "SELECT * FROM Albums WHERE Name = '%s' LIMIT 1;", albumName.c_str());
	executeSQL(buffer, Callbacks::listAlbums, (void*)&albumsList, db);
	if(albumsList.empty())
		throw MyException("No album with name " + albumName + " exists");
	linkAlbumsToPicturesToTags(albumsList);
	albumsLists.push_back(albumsList);
	return albumsLists.back().front();
}

void DatabaseAccess::closeAlbum(Album& pAlbum)
{
	
}

void DatabaseAccess::printAlbums()
{
	list<Album> albumsList;
	executeSQL("SELECT * FROM Albums;", Callbacks::listAlbums, (void*)&albumsList, db);
	if (albumsList.empty())
	{
		throw MyException("There are no existing albums.");
	}
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album : albumsList)
	{
		std::cout << std::setw(5) << "* " << album;
	}
}

// picture related
void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	snprintf(buffer, 300, "INSERT INTO Pictures (Name, Location, Creation_date, Album_id) VALUES ('%s', '%s', '%s', (SELECT Id FROM Albums WHERE Name = '%s'));", picture.getName().c_str(), picture.getPath().c_str(), picture.getCreationDate().c_str(), albumName.c_str());
	executeSQL(string(buffer), db);
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	snprintf(buffer, 300, "DELETE FROM Pictures WHERE Album_id = (SELECT Id FROM Albums WHERE Name = '%s') AND Name = '%s';", albumName.c_str(), pictureName.c_str());
	executeSQL(string(buffer), db);
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	snprintf(buffer, 300, "INSERT INTO Tags (User_id, Picture_id) VALUES (%d, (SELECT Id FROM Pictures WHERE Name = '%s' AND Album_id = (SELECT Id FROM Albums WHERE Name = '%s')));", userId, pictureName.c_str(), albumName.c_str());
	executeSQL(string(buffer), db);
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	snprintf(buffer, 300, "DELETE FROM Tags WHERE User_id = %d AND Picture_id = (SELECT Id FROM Pictures WHERE Name = '%s' AND Album_id = (SELECT Id FROM Albums WHERE Name = '%s'));", userId, pictureName.c_str(), albumName.c_str());
	executeSQL(string(buffer), db);
}

// user related
void DatabaseAccess::printUsers()
{
	vector<User> usersVector;
	executeSQL("SELECT * FROM Users", Callbacks::vecUsers, (void*)&usersVector, db);
	if(usersVector.empty())
		throw MyException("There are no existing users.");
	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (int i = 0; i < usersVector.size(); i++)
		cout << usersVector[i] << endl;
}

void DatabaseAccess::createUser(User& user)
{
	snprintf(buffer, 300, "INSERT INTO Users (Name) VALUES ('%s');", user.getName().c_str());
	executeSQL(string(buffer), db);
}

void DatabaseAccess::deleteUser(const User& user)
{
	snprintf(buffer, 300, "DELETE FROM Users WHERE Id = %d AND Name = '%s';", user.getId(), user.getName().c_str());
	executeSQL(buffer, db);
}

bool DatabaseAccess::doesUserExists(int userId)
{
	vector<User> usersVector;
	executeSQL("SELECT * FROM Users WHERE Id = " + to_string(userId) + ";", Callbacks::vecUsers, (void*)&usersVector, db);
	return !usersVector.empty();
}

User& DatabaseAccess::getUser(int userId)
{
	vector<User> usersVector;
	executeSQL("SELECT * FROM Users WHERE Id = " + to_string(userId) + ";", Callbacks::vecUsers, (void*)&usersVector, db);
	if(usersVector.empty())
		throw ItemNotFoundException("User", userId);
	users.push_back(usersVector.front());
	return users.back();
}

// user statistics
int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int albums = 0;

	snprintf(buffer, 300, "SELECT COUNT(Users.Id) FROM Users INNER JOIN Albums ON Users.Id = Albums.User_id WHERE Users.Id = %d;", user.getId());
	executeSQL(buffer, Callbacks::count, (void*)&albums, db);
	return albums;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	int count = 0;

	snprintf(buffer, 300, "SELECT COUNT(DISTINCT Albums.Id) FROM Users INNER JOIN Tags ON Users.Id = Tags.User_id INNER JOIN Pictures ON Tags.Picture_id = Pictures.Id INNER JOIN Albums ON Pictures.Album_id = Albums.Id WHERE Users.Id = %d;", user.getId());
	executeSQL(buffer, Callbacks::count, (void*)&count, db);
	return count;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	int tags = 0;

	snprintf(buffer, 300, "SELECT COUNT(Users.Id) FROM Users INNER JOIN Tags ON Users.Id = Tags.User_id WHERE Users.Id = %d;", user.getId());
	executeSQL(buffer, Callbacks::count, (void*)&tags, db);
	return tags;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int tags = 0, albums = 0;

	snprintf(buffer, 300, "SELECT COUNT(Users.Id) FROM Users INNER JOIN Tags ON Users.Id = Tags.User_id WHERE Users.Id = %d;", user.getId());
	executeSQL(buffer, Callbacks::count, (void*)&tags, db);

	snprintf(buffer, 300, "SELECT COUNT(Users.Id) FROM Users INNER JOIN Albums ON Users.Id = Albums.User_id WHERE Users.Id = %d;", user.getId());
	executeSQL(buffer, Callbacks::count, (void*)&albums, db);

	return (float)tags / albums;
}

int DatabaseAccess::maxUserId()
{
	int id = 0;
	executeSQL("SELECT COALESCE(MAX(Id), 0) FROM Users;", Callbacks::count, (void*)&id, db);
	return id;
}

int DatabaseAccess::maxPictureId()
{
	int id = 0;
	executeSQL("SELECT COALESCE(MAX(Id), 0) FROM Pictures;", Callbacks::count, (void*)&id, db);
	return id;
}

// queries
User& DatabaseAccess::getTopTaggedUser()
{
	vector<User> user;
	executeSQL("SELECT Users.* FROM Users LEFT JOIN Tags ON Users.Id = Tags.User_id GROUP BY Users.Id ORDER BY COUNT(Tags.User_id) DESC LIMIT 1;", Callbacks::vecUsers, (void*)&user, db);
	if(user.empty())
		throw MyException("ERROR: There are no users.\n");
	users.push_back(user.front());
	return users.back(); //please note that if there are multiple users tagged the same, one will be taken. top tagged user can have 0 tags 
}

Picture& DatabaseAccess::getTopTaggedPicture()
{
	list<Album> albumsList = getAlbums();
	int mostTags = -1;
	Picture* pMostTags = nullptr;

	for (auto const& album : albumsList)
	{
		for (auto const& picture : album.getPictures())
		{
			if (picture.getTagsCount() > mostTags)
			{
				mostTags = picture.getTagsCount();
				pMostTags = new Picture(picture);
			}
		}
	}

	if (mostTags == -1)
		throw MyException("ERROR: There are no pictures.\n");
	list<Picture> picturesList;
	picturesList.push_back(*pMostTags);
	picturesLists.push_back(picturesList);
	return picturesLists.back().front(); //same note like at getTopTaggedUser()
}

std::list<Picture>& DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	list<Album> albumsList = getAlbums();
	list<Picture> picturesList;

	for (auto const& album : albumsList)
	{
		for (auto const& picture : album.getPictures())
		{
			if (picture.isUserTagged(user))
				picturesList.push_back(picture);
		}
	}

	if (picturesList.empty())
		throw MyException("ERROR: Couldn't find any pictures that the user is tagged at.\n");
	picturesLists.push_back(picturesList);
	return picturesLists.back();
}


//auxiliary methods

void DatabaseAccess::executeSQL(string sqlStatement, sqlite3*& db)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		throw MyException(errMessage);
}

void DatabaseAccess::executeSQL(string sqlStatement, int (*callback)(void*, int, char**, char**), void* data, sqlite3*& db)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement.c_str(), callback, data, &errMessage);
	if (res != SQLITE_OK)
		throw MyException(errMessage);
}

void DatabaseAccess::linkAlbumsToPicturesToTags(list<Album>& albumsList)
{
	for (list<Album>::iterator it = albumsList.begin(); it != albumsList.end(); ++it)
	{
		snprintf(buffer, 300, "SELECT Pictures.* FROM Pictures INNER JOIN Albums ON Pictures.Album_id = Albums.Id WHERE Albums.Id = (SELECT Id FROM Albums WHERE User_id = %d);", it->getOwnerId());
		executeSQL(buffer, Callbacks::linkPicturesToAlbum, (void*) & (*it), db);

		for (list<Picture>::iterator picIt = (*it).getPicturesRef().begin(); picIt != (*it).getPicturesRef().end(); ++picIt)
		{
			snprintf(buffer, 300, "SELECT Tags.User_id FROM Tags INNER JOIN Pictures ON Tags.Picture_id = Pictures.Id WHERE Pictures.Id = %d;", picIt->getId());
			executeSQL(buffer, Callbacks::linkTagsToPicture, (void*) & (*picIt), db);
		}
	}
}