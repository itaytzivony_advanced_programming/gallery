#include <iostream>
#include <string>
#include <ctime> 
#include "MemoryAccess.h"
#include "DatabaseAccess.h"
#include "AlbumManager.h"

void welcomePrints();
int getCommandNumberFromUser();

int main(void)
	{
	// initialization data access
	//MemoryAccess dataAccess;

	// initialize database access
	DatabaseAccess dataAccess;

	// initialize album manager
	AlbumManager albumManager(dataAccess);


	std::string albumName;
	std::cout << "Welcome to Gallery!" << std::endl;
	welcomePrints();
	std::cout << "===================" << std::endl;
	std::cout << "Type " << HELP << " to a list of all supported commands" << std::endl;
	
	do
	{
		int commandNumber = getCommandNumberFromUser();
		
		try
		{
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		}
		catch (std::exception& e)
		{
			if (string(e.what()) == "FOREIGN KEY constraint failed")
				cout << "ERROR: Failed to remove entity, please remove any linked entities first (Example: remove all tags from picture before removing the picture)." << endl;
			else
				std::cout << e.what() << std::endl;
		}
	} 
	while (true);
}


void welcomePrints()
{
	std::time_t result = std::time(nullptr);
	std::cout << "By: Itay Tzivony" << std::endl << "Date and time: " << std::asctime(std::localtime(&result));
}

int getCommandNumberFromUser()
{
	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("0123456789");

	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);

	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number only!" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}

	return std::atoi(input.c_str());
}