#include "Callbacks.h"

int Callbacks::count(void* data, int argc, char** argv, char** azColName)
{
	int* count = (int*)data;
	*count = atoi(argv[0]);
	return 0;
}

int Callbacks::listAlbums(void* data, int argc, char** argv, char** azColName)
{
	list<Album>* pAlbumsList = (list<Album>*)data;
	int user_id = 0;
	string name, creation_date;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "User_id")
		{
			user_id = atoi(argv[i]);
		}
		else if (string(azColName[i]) == "Name")
		{
			name = argv[i];
		}
		else if (string(azColName[i]) == "Creation_date")
		{
			creation_date = argv[i];
		}
	}
	pAlbumsList->push_back(Album(user_id, name, creation_date));
	return 0;
}

int Callbacks::linkPicturesToAlbum(void* data, int argc, char** argv, char** azColName)
{
	Album* pAlbum = (Album*)data;
	int id = 0;
	string name, location, creation_date;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "Id")
		{
			id = atoi(argv[i]);
		}
		else if (string(azColName[i]) == "Name")
		{
			name = argv[i];
		}
		else if (string(azColName[i]) == "Location")
		{
			location = argv[i];
		}
		else if (string(azColName[i]) == "Creation_date")
		{
			creation_date = argv[i];
		}
	}
	pAlbum->addPicture(Picture(id, name, location, creation_date));
	return 0;
}

int Callbacks::linkTagsToPicture(void* data, int argc, char** argv, char** azColName)
{
	Picture* pPicture = (Picture*)data;
	pPicture->tagUser(atoi(argv[0]));
	return 0;
}

int Callbacks::vecUsers(void* data, int argc, char** argv, char** azColName)
{
	vector<User>* pUsersVector = (vector<User>*)data;
	int id = 0;
	string name;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "Id")
		{
			id = atoi(argv[i]);
		}
		else if (string(azColName[i]) == "Name")
		{
			name = argv[i];
		}
	}
	pUsersVector->push_back(User(id, name));
	return 0;
}